<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ingrid Mat</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{{ asset('assets/app.css') }}}">
</head>
<body>
    @include('components.videosStatusBubble')
   <div>
       @include('components.navbar')
       @include('components.carousel',['banners'=>$banners])
       <div style="display: none" class="advert-instruction">
        <div class="center-block-children " style="flex-direction: column">
            <div>
            <h5 style="display: flex;align-items:center;justify-content:center;padding: 1.5rem;"> <div class="label-card shadow">{{$videoQuota[0]['value']}}</div>vidéos vous donnent</h5>
            </div>
            <div class="pricing-text-2">
                <h5><div class="label-card shadow">{{$completionPrice[0]['value']}}</div>FCFA regardez, remplisser puis gagner</h5>
            </div>
        </div>
       </div>
       <div class="main-advert-container disapear">
           @include('components.video',['url'=>URL::to("stream/".$mainAdvert[0]['url']),'mainAdvert'=>false,'autoplay'=>true])
       </div>
       <div class="contain-title shaped disapear">
           <div class="div-title">
               Nos partenaires
           </div>
           <div class="shaped">
           </div>
       </div>
       <div class="contain-description disapear">
           <div class="partners">
               @foreach ($partners as $partner)
                    <div style="background-image: url('{{asset('storage/'.$partner['url'])}}')">

                    </div>
               @endforeach
           </div>
       </div>
       <div class="contain-description disapear">
           <h4>
               Comment ça fonctionne ?
           </h4>
       </div>
       <div class="contain-title contain-title-normal-background contain-tips disapear">
           <div class="div-title">
              <div class="explanations">
                  <div class="explanation-item">
                      <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                            <path id="checked" d="M26,1A25,25,0,1,0,51,26,25.027,25.027,0,0,0,26,1Zm0,4.167A20.833,20.833,0,1,1,5.167,26,20.8,20.8,0,0,1,26,5.167Zm8.724,8.464a1.134,1.134,0,0,0-.781.521L23.721,29.32,18.9,24.568a1.07,1.07,0,0,0-1.628-.26L15.388,26.2a1.579,1.579,0,0,0,0,1.888l7.292,7.292a3.765,3.765,0,0,0,1.628.651,1.882,1.882,0,0,0,1.5-.846L38.3,16.82c.415-.627.179-1.213-.651-1.628L35.57,13.7A1.508,1.508,0,0,0,34.724,13.63Z" transform="translate(-1 -1)" fill="#bccd9e"/>
                        </svg>
                      </div>
                      <div class="explanation-number">
                          1
                      </div>
                      <div class="explanation-description">
                          Définissez votre catégorie
                      </div>
                  </div>
                  <div class="explanation-item">
                      <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                            <path id="checked" d="M26,1A25,25,0,1,0,51,26,25.027,25.027,0,0,0,26,1Zm0,4.167A20.833,20.833,0,1,1,5.167,26,20.8,20.8,0,0,1,26,5.167Zm8.724,8.464a1.134,1.134,0,0,0-.781.521L23.721,29.32,18.9,24.568a1.07,1.07,0,0,0-1.628-.26L15.388,26.2a1.579,1.579,0,0,0,0,1.888l7.292,7.292a3.765,3.765,0,0,0,1.628.651,1.882,1.882,0,0,0,1.5-.846L38.3,16.82c.415-.627.179-1.213-.651-1.628L35.57,13.7A1.508,1.508,0,0,0,34.724,13.63Z" transform="translate(-1 -1)" fill="#bccd9e"/>
                        </svg>
                      </div>
                      <div class="explanation-number">
                          2
                      </div>
                      <div class="explanation-description">
                          Regardez les pubs, Répondez aux formulaires
                      </div>
                  </div>
                  <div class="explanation-item">
                      <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                            <path id="checked" d="M26,1A25,25,0,1,0,51,26,25.027,25.027,0,0,0,26,1Zm0,4.167A20.833,20.833,0,1,1,5.167,26,20.8,20.8,0,0,1,26,5.167Zm8.724,8.464a1.134,1.134,0,0,0-.781.521L23.721,29.32,18.9,24.568a1.07,1.07,0,0,0-1.628-.26L15.388,26.2a1.579,1.579,0,0,0,0,1.888l7.292,7.292a3.765,3.765,0,0,0,1.628.651,1.882,1.882,0,0,0,1.5-.846L38.3,16.82c.415-.627.179-1.213-.651-1.628L35.57,13.7A1.508,1.508,0,0,0,34.724,13.63Z" transform="translate(-1 -1)" fill="#bccd9e"/>
                        </svg>
                      </div>
                      <div class="explanation-number">
                          3
                      </div>
                      <div class="explanation-description">
                          Copier votre code bonus
                      </div>
                  </div>
              </div>
           </div>
       </div>
       <div class="contain-description">
           <div class="disapear">
               <h4>
                   Pour commencer définissez vos critères.
               </h4>
           </div>
        <div id="criteria-form-container" class="disapear">
            <div class="criteria-form">
                <div class="criteria-form-item">
                    <div class="form-component">
                        @include('components.checkbox',['checkboxId'=>0])
                    </div>
                    <div class="form-label">
                        <h6>Homme</h6>
                    </div>
                </div>
                <div class="criteria-form-item">
                    <div class="form-component">
                        @include('components.checkbox',['checkboxId'=>1])
                    </div>
                    <div class="form-label">
                        <h6>Femme</h6>
                    </div>
                </div>
                <div class="criteria-form-item">
                    <div class="form-label">
                        <h6>Age: </h6>
                    </div>
                    <div class="form-component">
                        @include('components.slider')
                    </div>
                </div>
            </div>
            <div class="button-container">
               <button class="btn btn-primary shadow" id="search-adverts">
                   Rechercher
               </button>
            </div>
        </div>
        <div class="adverts-container"></div>
        {{-- @include('components.loader') --}}
        {{-- @include('components.advertsList') --}}
            <!-- Modal -->
            <div class="modal fade advert-modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered modal-advert" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Loading..</h5><span>.</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                    <div class="advert-content" id="advert-content">
                        <div class="advert">
                            <div id="advertVideo" style="display: none">
                                @include('components.video',['url'=>"","mainAdvert"=>true,"autoplay"=>false])
                            </div>
                            <div id="advertImg" style="display: none">
                                <img src=" {{ "http://localhost:8000/assets/images/image1.png" }}" alt="" srcset="">
                            </div>
                        </div>
                        <div class="questions">
                            <div class="questionnaire" id="questionnaire">
                                <div class="questionnaire-instruction" id="questionnaire-instruction">Visionnez puis remplissez le formulaire ci dessous</div>
                                {{-- @include('components.questionList') --}}
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <button id="submit-questionnaire" type="button" class="btn btn-primary">Soumettre</button>
                    </div>
                </div>
                </div>
            </div>

        </div>
        <div class="footer contain-title-normal-background">
            <div class="footer-item">
                <div class="footer-item-title">
                    REJOIGNEZ-NOUS
                </div>
                <div>
                    <div>
                        L'Equipe
                    </div>
                    <div>
                        L'Etrepise
                    </div>
                </div>
            </div>
            <div class="footer-item footer-news">
                <div class="footer-item-title">
                    NOUVEAUTES
                </div>
                <div>
                    <div class="footer-news-item">
                        <div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                    <path id="video_playlist" d="M10.75,4V6.25h13.5V4ZM7.375,7.375v2.25h20.25V7.375ZM4,10.75V31H31V10.75ZM6.25,13h22.5V28.75H6.25Zm7.875,1.828V26.922l8.438-5.1,1.617-.949-1.617-.949Zm2.25,3.973,3.481,2.074-3.481,2.074Z" transform="translate(-4 -4)" fill="#fff"/>
                                </svg>
                            </div>
                        </div>
                        <div>
                            <div>
                                Les tarifs sont fixés à un prix optimal!!
                            </div>
                            <div>
                                9 Novembre 2020
                            </div>
                        </div>
                    </div>
                    <div class="footer-news-item">
                        <div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                    <path id="video_playlist" d="M10.75,4V6.25h13.5V4ZM7.375,7.375v2.25h20.25V7.375ZM4,10.75V31H31V10.75ZM6.25,13h22.5V28.75H6.25Zm7.875,1.828V26.922l8.438-5.1,1.617-.949-1.617-.949Zm2.25,3.973,3.481,2.074-3.481,2.074Z" transform="translate(-4 -4)" fill="#fff"/>
                                </svg>
                            </div>
                        </div>
                        <div>
                            <div>
                                Mis à jour des publicités
                            </div>
                            <div>
                                1 Novembre 2020
                            </div>
                        </div>
                    </div>
                    <div class="footer-news-item">
                        <div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                    <path id="video_playlist" d="M10.75,4V6.25h13.5V4ZM7.375,7.375v2.25h20.25V7.375ZM4,10.75V31H31V10.75ZM6.25,13h22.5V28.75H6.25Zm7.875,1.828V26.922l8.438-5.1,1.617-.949-1.617-.949Zm2.25,3.973,3.481,2.074-3.481,2.074Z" transform="translate(-4 -4)" fill="#fff"/>
                                </svg>
                            </div>
                        </div>
                        <div>
                            <div>
                                Désormais dispo à l'ouest
                            </div>
                            <div>
                                3 Octorbre 2020
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-more">
                <div class="footer-item ">
                    <div class="footer-item-title">
                        GENERAL
                    </div>
                    <div>
                        Termes et Contrat d'utilisation
                    </div>
                    <div>
                        FAQ
                    </div>
                    <div>
                        Contacts
                    </div>
                </div>
                <div>
                    <div class="footer-item-title">
                        FOLLOW
                    </div>
                    <div class="social-network-container">
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                viewBox="0 0 24 24" fill="#FFFFFF">
                                <path
                                    d="M12 2C6.4889971 2 2 6.4889971 2 12C2 17.511003 6.4889971 22 12 22C17.511003 22 22 17.511003 22 12C22 6.4889971 17.511003 2 12 2 z M 12 4C16.430123 4 20 7.5698774 20 12C20 16.014467 17.065322 19.313017 13.21875 19.898438L13.21875 14.384766L15.546875 14.384766L15.912109 12.019531L13.21875 12.019531L13.21875 10.726562C13.21875 9.7435625 13.538984 8.8710938 14.458984 8.8710938L15.935547 8.8710938L15.935547 6.8066406C15.675547 6.7716406 15.126844 6.6953125 14.089844 6.6953125C11.923844 6.6953125 10.654297 7.8393125 10.654297 10.445312L10.654297 12.019531L8.4277344 12.019531L8.4277344 14.384766L10.654297 14.384766L10.654297 19.878906C6.8702905 19.240845 4 15.970237 4 12C4 7.5698774 7.5698774 4 12 4 z"
                                    fill="#FFFFFF" />
                            </svg>
                        </div>
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                viewBox="0 0 24 24" fill="#56ABEE">
                                <path style="fill:white"
                                    d="M24 4.300781C23.101563 4.699219 22.199219 5 21.199219 5.101563C22.199219 4.5 23 3.5 23.398438 2.398438C22.398438 3 21.398438 3.398438 20.300781 3.601563C19.300781 2.601563 18 2 16.601563 2C13.898438 2 11.699219 4.199219 11.699219 6.898438C11.699219 7.300781 11.699219 7.699219 11.800781 8C7.699219 7.800781 4.101563 5.898438 1.699219 2.898438C1.199219 3.601563 1 4.5 1 5.398438C1 7.101563 1.898438 8.601563 3.199219 9.5C2.398438 9.398438 1.601563 9.199219 1 8.898438C1 8.898438 1 8.898438 1 9C1 11.398438 2.699219 13.398438 4.898438 13.800781C4.5 13.898438 4.101563 14 3.601563 14C3.300781 14 3 14 2.699219 13.898438C3.300781 15.898438 5.101563 17.300781 7.300781 17.300781C5.601563 18.601563 3.5 19.398438 1.199219 19.398438C0.800781 19.398438 0.398438 19.398438 0 19.300781C2.199219 20.699219 4.800781 21.5 7.5 21.5C16.601563 21.5 21.5 14 21.5 7.5C21.5 7.300781 21.5 7.101563 21.5 6.898438C22.5 6.199219 23.300781 5.300781 24 4.300781"
                                    fill="#56ABEE" />
                            </svg>
                        </div>
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                viewBox="0 0 24 24" fill="#FFFFFF">
                                <path
                                    d="M5 3C3.895 3 3 3.895 3 5L3 19C3 20.105 3.895 21 5 21L19 21C20.105 21 21 20.105 21 19L21 5C21 3.895 20.105 3 19 3L5 3 z M 5 5L19 5L19 19L5 19L5 5 z M 7.7792969 6.3164062C6.9222969 6.3164062 6.4082031 6.8315781 6.4082031 7.5175781C6.4082031 8.2035781 6.9223594 8.7167969 7.6933594 8.7167969C8.5503594 8.7167969 9.0644531 8.2035781 9.0644531 7.5175781C9.0644531 6.8315781 8.5502969 6.3164062 7.7792969 6.3164062 z M 6.4765625 10L6.4765625 17L9 17L9 10L6.4765625 10 z M 11.082031 10L11.082031 17L13.605469 17L13.605469 13.173828C13.605469 12.034828 14.418109 11.871094 14.662109 11.871094C14.906109 11.871094 15.558594 12.115828 15.558594 13.173828L15.558594 17L18 17L18 13.173828C18 10.976828 17.023734 10 15.802734 10C14.581734 10 13.930469 10.406562 13.605469 10.976562L13.605469 10L11.082031 10 z"
                                    fill="#FFFFFF" />
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
   <!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-auth.js "></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-analytics.js"></script>

<script>
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyAhq6JUYbpmDlHvQ3pxtGQ0IbXj3IBrkQ0",
    authDomain: "ingridmat-29b93.firebaseapp.com",
    databaseURL: "https://ingridmat-29b93.firebaseio.com",
    projectId: "ingridmat-29b93",
    storageBucket: "ingridmat-29b93.appspot.com",
    messagingSenderId: "318817354139",
    appId: "1:318817354139:web:2e5bbcb3733d452d01e13e",
    measurementId: "G-XQQCC92JC9"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();

</script>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.0/moment.min.js" integrity="sha512-Izh34nqeeR7/nwthfeE0SI3c8uhFSnqxV0sI9TvTcXiFJkMd6fB644O64BRq2P/LA/+7eRvCw4GmLsXksyTHBg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-duration-format/2.3.2/moment-duration-format.min.js" integrity="sha512-ej3mVbjyGQoZGS3JkES4ewdpjD8UBxHRGW+MN5j7lg3aGQ0k170sFCj5QJVCFghZRCio7DEmyi+8/HAwmwWWiA==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/gsap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/ScrollTrigger.min.js"></script>
<script src="{{asset("assets/js/app.js")}}"></script>
</body>
</html>

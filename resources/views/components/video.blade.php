<div class="container video-container">
    <div class="c-video">
        <video @if ($autoplay == true)
            {{ 'autoplay' }}
            @endif
             src="{{ $url }}" class="video

            @if ($mainAdvert == true)
                {{ 'main' }}
            @endif

            " controls></video>
        {{-- <div class="controls">
            <div class="video-progress-bar">
                <div class="video-total-progress"></div>
            </div>
            <div class="buttons">
                <button id="play-pause-button">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                        viewBox="0 0 24 24" fill="#FFFFFF">
                        <path d="M8,5v14l11-7L8,5z" fill="#FFFFFF" />
                    </svg>
                </button>
            </div>
            <div class="time">
                <div class="esplaned">
                    00:00
                </div>/
                <div class="total">
                    00:00
                </div>
            </div>
        </div> --}}
    </div>
</div>

<div class="slider-container">
    <div class="middle">
        <div class="multi-range-slider">
            <input type="range" id="input-left" min="0" max="100" value="0">
            <input type="range" id="input-right" min="0" max="100" value="75">
            <div class="slider">
                <div class="track"></div>
                <div class="range"></div>
                {{-- <div class="thumb left"></div> --}}
                <div class="thumb right"></div>
            </div>
        </div>
    </div>
    <div class="values">
        <h6 id="leftValue" style="display: none">0</h6>
        <h6 style="display: none">-</h6>
        <h6 id="rightValue">100</h6>
        <h6 style="margin-left: 0.1rem"> ans</h6>
    </div>
</div>

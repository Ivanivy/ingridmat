<div class="adverts-container">
    @csrf
    @if (count($adverts) > 0)
        @foreach ($adverts as $advert)
            @if ($advert['type'] == 0)
                @include('components.advertItem',['type'=>'image','src'=>asset('storage/'.$advert['url']),'entrepriseName'=>"stella
                couture",'title'=>$advert['titre'],'date'=>$advert['updated_at'],'id'=>$advert['id']])
            @endif

            @if ($advert['type'] == 1)
                @include('components.advertItem',['type'=>'video','src'=>URL::to("stream/".$advert['url']),'entrepriseName'=>$advert['entreprise']['nom'],'title'=>$advert['titre'],'date'=>$advert['updated_at'],'id'=>$advert['id']])
            @endif
        @endforeach
    @else
        <h4>Pas de publicité trouvée pour cette catégorie</h4>
    @endif

</div>

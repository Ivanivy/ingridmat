<div id="questions-list">
    @foreach ($questionnaire['questions'] as $key => $question)
        @include('components.questionItem',['questionNumber'=>$key+1,'question'=>$question])
    @endforeach
</div>

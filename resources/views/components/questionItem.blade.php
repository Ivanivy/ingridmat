<div class="question-item" data-questionid="{{$question['id']}}" data-name="inlineRadioOptions{{ $questionNumber }}">
    <h6> {{ $questionNumber }}) {{ $question['formulation'] }}</h6>
    <div style="display: flex;flex-direction: row">
        @foreach ($question['possibilites'] as $possibilite)
            <div class="form-check possibilite form-check-inline">
                <input class="form-check-input" type="radio" name="inlineRadioOptions{{ $questionNumber }}"
                    id="inlineRadio{{ $questionNumber }}" value="{{ $possibilite['id'] }}">
                <label class="form-check-label" for="inlineRadio1">{{ $possibilite['formulation'] }}</label>
            </div>
        @endforeach
    </div>
</div>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active"
            style="background-image:linear-gradient(to bottom, rgba(0,0,0,0.82) 0%,rgba(0,0,0,0.82) 100%), url('{{ asset('assets/images/image1.png') }}')">
            <div class="container">
                <h1>SOS Crédit ou Data ?
                    Regardez des pubs et gagnez !!!</h1>
            </div>
        </div>
        @foreach ($banners as $banner)
            <div class="carousel-item"
                style="background-image:linear-gradient(to bottom, rgba(0,0,0,0.82) 0%,rgba(0,0,0,0.82) 100%), url('{{ asset('storage/'.$banner['url']) }}')">
                <div class="container">
                    <h1>{{ $banner['value'] }}</h1>
                </div>
            </div>
        @endforeach
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

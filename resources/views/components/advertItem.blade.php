<div class="advert-item" data-type="{{ $type }}" data-entreprise="{{ $entrepriseName }}" data-title="{{ $title }}"
    data-pubid="{{ $id }}" data-src="{{ $src }}" data-toggle="modal" data-target="#exampleModal">
    <div>
        @if ($type == 'video')
            <video muted loop id="myVideo">
                <source src="{{ $src }}">
            </video>
        @else
            <img class="img-fluid" id="myImage" src=" {{ $src }}" alt="" srcset="">
        @endif
    </div>
    <div>
        <div class="entreprise-name">
            <div>
                {{ $entrepriseName }}
            </div>
            <div>
                @if ($type == 'video')
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                        viewBox="0 0 24 24" fill="#FFFFFF">
                        <path
                            d="M20,3H4C2.897,3,2,3.897,2,5v14c0,1.103,0.897,2,2,2h16c1.103,0,2-0.897,2-2V5C22,3.897,21.103,3,20,3z M9,16V8l7,4L9,16z"
                            fill="#FFFFFF" />
                    </svg>
                @else
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                        viewBox="0 0 24 24" fill="#FFFFFF">
                        <path
                            d="M20,4H4C2.895,4,2,4.895,2,6v12c0,1.105,0.895,2,2,2h16c1.105,0,2-0.895,2-2V6C22,4.895,21.105,4,20,4z M10,8 c0.552,0,1,0.448,1,1c0,0.552-0.448,1-1,1S9,9.552,9,9C9,8.448,9.448,8,10,8z M19,17H5l3.499-4.499L11,15.51l3.5-4.509L19,17z"
                            fill="#FFFFFF" />
                    </svg>
                @endif
            </div>
        </div>
        <div class="title">
            {{ $title }}
        </div>
        <div class="publication-date">
            publié le {{ $date }}
        </div>
    </div>
</div>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">
        <div>
            <svg xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                <path id="video_playlist"
                    d="M10.75,4V6.25h13.5V4ZM7.375,7.375v2.25h20.25V7.375ZM4,10.75V31H31V10.75ZM6.25,13h22.5V28.75H6.25Zm7.875,1.828V26.922l8.438-5.1,1.617-.949-1.617-.949Zm2.25,3.973,3.481,2.074-3.481,2.074Z"
                    transform="translate(-4 -4)" fill="#fff" />
            </svg>
        </div>
        <div>
            IngridMat
        </div>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
        aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active" id="home-link">
                <a class="nav-link" href="http://localhost:8000">Acceuil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">A propos</a>
            </li>
        </ul>
    </div>
    <div class="header">
        <div class="progress-container">
            <div class="progress-bar" id="myBar"></div>
        </div>
    </div>
</nav>

const baseUrl = ` http://localhost:8000`;
// const baseUrl = "http://2ccb80004ea3.ngrok.io"
let loading3 = `
<div class="load-wrapp">
<div class="load-3">
  <div class="line"></div>
  <div class="line"></div>
  <div class="line"></div>
</div>
</div>
`;

if (localStorage.getItem("numero")) {
    $("#home-link").after(`
        <li class="nav-item">
          <a class="nav-link" href="#">${localStorage.getItem("numero")}</a>
        </li>
        <li class="nav-item " id="logout-link">
          <a class="nav-link" href="#">Se déconnecter</a>
        </li>
    `);
    $("#logout-link").click(function() {
        localStorage.removeItem("token");
        localStorage.removeItem("numero");
        document.location.reload();
    });
}


function setupProgressScrollBarIndicator() {
    var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    var scrolled = (winScroll / height) * 100;
    document.getElementById("myBar").style.width = scrolled + "%";
}

window.onscroll = function() { setupProgressScrollBarIndicator(); };

function showBubble() {
    if (localStorage.getItem("numero")) {
        fetchRemaingVideos().then(result => {
            console.log("remaining videos");
            console.log(result);
            $("#statistics-buble #videos-left").html(result);
            $("#statistics-buble").css("display", "block");
            $("#statistics-buble").removeClass("translator");
            setTimeout(function() {
                $("#statistics-buble").addClass("translator");
                $("#statistics-buble").css("display", "none");
            }, 8000)
        }).catch(error => {
            console.error(error);
        })
    }
}

showBubble();

async function submitPhoneNumberAuth() {
    var phoneNumber = window.phoneNumber;
    var appVerifier = window.recaptchaVerifier;
    try {
        console.log(phoneNumber);
        window.confirmationResult = await firebase
            .auth()
            .signInWithPhoneNumber(phoneNumber, appVerifier);
        console.log("terminé");
    } catch (error) {
        console.log(error);
    }
}

//function for fecthing remaining videos
async function fetchRemaingVideos() {
    return new Promise((resolve, reject) => {
        let data = {
            "phoneNumber": localStorage.getItem("numero"),
        };
        console.log(data);
        fetch(baseUrl + "/api/publicites/remaining", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                //add token to the header is exists
                [localStorage.getItem("token") ? 'app_token' : null]: localStorage.getItem("token")
            },
            body: JSON.stringify(data)
        }).then((response) => {
            response.json()
                .then((result) => {
                    console.log(result);
                    resolve(result.data.remainingVideos);
                }).catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    })
}

//function for fecthing remaining videos
async function checkUserPending() {
    return new Promise((resolve, reject) => {
        let data = {
            "phoneNumber": localStorage.getItem("numero"),
        };
        console.log(data);
        fetch(baseUrl + "/api/publicites/waitingtime", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                //add token to the header is exists
                [localStorage.getItem("token") ? 'app_token' : null]: localStorage.getItem("token")
            },
            body: JSON.stringify(data)
        }).then((response) => {
            response.json()
                .then((result) => {
                    console.log(result.data);
                    resolve(result.data);
                }).catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    })
}

function showResults(result) {
    if (result.data.wait) {
        $(".fill-informations-container").html(`
        <form id="calling-number-form" style="display:flex;flex-direction:column;align-items:center">
        <label  for="verification-code">Vous devez attendre:</label>
        <h2 style="margin-left: auto;
        display: flex;
        text-align:center;
        color: var(--primaryColor);
        justify-content: center;" id="waiting-time"><h2>
        </div>
        </form>
        `);
        seconds = result.data.waiting_time;
        setInterval(() => {
            $("#waiting-time").html(moment.duration(seconds, "seconds").format("d [jours], h [heures], m [minutes], s [secs] "));
            console.warn("bonjour");
            seconds--;
        }, 1000);
    } else if (result.data.seen_status == true) {
        $(".fill-informations-container").html(`
        <form id="calling-number-form" style="display:flex;flex-direction:column;align-items:center">
        <label  for="verification-code">Désolé vous avez déjà visionné cette pub</label>
        </div>
        </form>
        `);
    } else if (!result.code) {
        let hint = null;
        //we concactenate differents number of failed answers
        if (result.failedAnswers.length != 0) {
            let hintString = "";
            result.failedAnswers.forEach(element => {
                hintString += element + ", ";
            })
            hint = `<div style="color:red;">${hintString} sont incorrects</div> `
        }
        $(".fill-informations-container").html(`
        <form id="calling-number-form">
        ${hint?hint:""}
        <label for="verification-code">Score: <strong> ${result.score ? result.score : ""}%</strong></label>
        <label for="verification-code">Vidéos restantes:</label>
        <h2 style="margin-left: auto;
        display: flex;
        color: var(--primaryColor);
        justify-content: center;">${result.remainingVideos}</h2>
        </div>
        </form>
        `);
    } else {
        $(".fill-informations-container").html(`
        <form id="calling-number-form">
        <label for="verification-code">Bravo!! votre code bonus:</label>
        <h2 style="margin-left: auto;
        display: flex;
        color: var(--primaryColor);
        justify-content: center;">${result.code}</h2>
        </div>
        </form>
        `);
    }
    $("#statistics-buble").css("display", "block");
    $("#statistics-buble").removeClass("translator");
    setTimeout(function() {
        $("#statistics-buble").addClass("translator");
        $("#statistics-buble").css("display", "none");
    }, 30000)
}

/*adding animation on screen of differents elements*/
gsap.from(".partners", {
    scrollTrigger: ".partners",
    x: 1000,
    opacity: 0,
    ease: "power4",
    duration: 1
});

gsap.from(".contain-description", {
    scrollTrigger: ".contain-description",
    opacity: 0,
    ease: "power4",
    duration: 1
});
gsap.from(".explanation-item", {
    scrollTrigger: ".explanations",
    y: -100,
    opacity: 0,
    stagger: 0.7,
    ease: "power4",
    duration: 1
});
gsap.from("#criteria-form-container", {
    scrollTrigger: "#criteria-form-container",
    opacity: 0,
    ease: "sine",
    duration: 2.5
});
/*end adding  elements animation*/

$(document).ready(function() {
    /*scripts for slider*/
    let criteriaFormValues = {
        leftValue: 0,
        rightValue: 0,
        male: false,
        female: false,
    }
    var inputLeft = document.getElementById("input-left");
    var inputRight = document.getElementById("input-right");
    var rightValue = document.querySelector("#rightValue");
    var thumbRight = document.querySelector(".slider > .thumb.right");
    var range = document.querySelector(".slider > .range");



    function setRightValue() {
        var _this = inputRight,
            min = parseInt(_this.min),
            max = parseInt(_this.max);

        _this.value = Math.max(parseInt(_this.value), parseInt(inputLeft.value) + 1);
        criteriaFormValues.rightValue = _this.value;
        rightValue.innerHTML = _this.value;
        var percent = ((_this.value - min) / (max - min)) * 100;

        thumbRight.style.right = (100 - percent) + "%";
        range.style.right = (100 - percent) + "%";
    }
    setRightValue();

    inputRight.addEventListener("input", setRightValue);

    inputRight.addEventListener("mouseover", function() {
        thumbRight.classList.add("hover");
    });
    inputRight.addEventListener("mouseout", function() {
        thumbRight.classList.remove("hover");
    });
    inputRight.addEventListener("mousedown", function() {
        thumbRight.classList.add("active");
    });
    inputRight.addEventListener("mouseup", function() {
        thumbRight.classList.remove("active");
    });

    var maleCheckBox = document.querySelector("#check0");
    var femaleChackbox = document.querySelector("#check1");

    maleCheckBox.addEventListener("click", function(e) {
        criteriaFormValues.male = e.target.checked;
    })
    femaleChackbox.addEventListener("click", function(e) {
        criteriaFormValues.female = e.target.checked;
    })

    //fixed navbar effect
    var offset = $(".navbar").offset().top;
    $(document).scroll(function() {
        var scrollTop = $(document).scrollTop();
        if (scrollTop > offset) {
            $(".navbar").addClass('sticky');
        } else {
            $(".navbar").removeClass('sticky');
        }
    });


    //setup for modal
    function setupModal() {
        //defining thing
        let advertSrc;
        let type;
        let title;
        let entreprise;
        let pubId;

        // when the modal is opened autoplay it
        $('#exampleModal').on('shown.bs.modal', function(e) {
            // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
            console.log("launching video");
            if (type == "image") {
                $("#advertImg").css('display', 'flex');
                console.log(advertSrc);
                $("#advertImg").find("img").attr('src', advertSrc);
                console.log("image taken")
            }
            if (type == "video") {
                $("#advertVideo").css('display', 'flex');
                $(".video.main").attr('src', advertSrc);
                video = document.querySelector(".video.main");
                video.play();
                console.log("video taken");
            }
            $(".advert-modal .modal-header h5").html(title);
            $(".advert-modal .modal-header> span").html("(" + entreprise + ")");
            $("#submit-questionnaire").show();
        });
        // stop playing the youtube video when I close the modal
        $('#exampleModal').on('hide.bs.modal', function(e) {
            // a poor man's stop video
            $(".video.main").attr('src', "");
            $("#advertVideo").css('display', 'none');
            $("#advertImg").css('display', 'none');
            $("#loading-advert-modal").remove();
            $(".fill-informations-container").remove();
            video = document.querySelector(".video.main");
            video.addEventListener('ended', function() {
                this.currentTime = 0;
                this.play();
            }, false);
            video.play();
            $(".questions").children().not(":first-child").remove();
            $("html, body").animate({ scrollTop: 0 }, 1000);
            showBubble();
        });

        // Gets the video src from the data-src on each button
        $('.advert-item').click(function() {
            $("#questionnaire").after(`
            <div class="preloader" id="loading-advert-modal">
                    <div class="spinner"></div>
                    <span id="loading-msg">Loading...</span>
            </div>
            `);
            advertSrc = $(this).data("src");
            type = $(this).data("type");
            title = $(this).data("title");
            entreprise = $(this).data("entreprise");
            pubId = $(this).data("pubid");
            window.pubId = pubId;
            fetch(baseUrl + "/questionnaires/" + pubId, {
                    headers: {
                        'Content-Type': 'application/json;charset=utf-8',
                        //add token to the header is exists
                        [localStorage.getItem("token") ? 'app_token' : null]: localStorage.getItem("token")
                    }
                })
                .then(response => {
                    return response.text();
                }).then(function(html) {
                    var parser = new DOMParser();
                    var doc = parser.parseFromString(html, 'text/html');
                    let questionsLIst = doc.querySelector("#questions-list")
                    $("#questions-list").remove();
                    $("#loading-advert-modal").remove();
                    $("#questionnaire").after(questionsLIst);
                });
        });
    }

    //script for getting values from mcq

    // This function runs when the 'sign-in-button' is clicked
    // Takes the value from the 'phoneNumber' input and sends SMS to that phone number

    $("#submit-questionnaire").click(function() {
        let soumission = {};
        /*hiding the questionnaire*/
        $(this).hide();
        /*getting selected values from the questionnaire for each id of the  question of the questionnaire we associate the id of the answared value*/
        $("#questions-list .question-item").each(function(each, element) {
            console.log($(element).data("name"));
            let val = $(element).find(`input[name='${$(element).data("name")}']:checked`).val();
            soumission[$(element).data("questionid")] = val;
        });
        console.log(soumission);
        /*we just attach the soumission to make it available from everywhere of the code */
        window.soumission = soumission;
        /*animation to fade the questionnaire and remove it from the dom*/
        gsap.fromTo("#questions-list", { opacity: 1 }, {
            opacity: 0,
            duration: 1,
            onComplete: function() {
                /*while the animation is complete an questionnaire invisible we just remove it */
                $("#questions-list").remove();
                /*we just change the title of the questionnaire*/
                $("#questionnaire-instruction").text("Entrez les informations pour terminer les étapes")
                    /*we just add the questionnaire loading animation*/
                $("#questionnaire").after(`
                    <div class="preloader" id="waiting-submission">
                            <div class="spinner"></div>
                            <span id="loading-msg">Loading...</span>
                    </div>
                `);
                let count = 30;
                let handleCounting = setInterval(function() {
                    count--;
                    $("#waiting-submission #loading-msg").html(`<h4>${count}</h4>`)
                }, 1000);

                window.myTimeout = setTimeout(() => {
                    clearInterval(handleCounting);
                    handleCounting = 0;
                    /*after counting we just remove the loading animation*/
                    $("#waiting-submission").remove();
                    /*then we put the div wich will contain the information*/
                    $("#questionnaire").after(`
                        <div id="finalising-steps">
                            <div class="fill-informations-container shadow">

                            </div>
                        <div>
                    `);
                    /*sorry sorry sorry i say again under presure i put all the functionnalities inside one controller */
                    /*and she sometimes changes a whole part of our accord but i should be humble in Jesus Name :D*/
                    /*so the route functions like this, first time if the user doesnt have a token or is not yet registered, the controller*/
                    /* will save the user and responsd with a token, then after anonymously authenticated, he just have to resend again, the request to
                    /* the same route to submit really the questionnaire*/

                    /****first if the token is not present, so the user is not authenticated*/
                    if (!localStorage.getItem("token") || !localStorage.getItem("numero")) {
                        /*first we display the calling number form*/
                        $(".fill-informations-container").html(`
                            <form id="calling-number-form">
                                <label for="callingNumber">Numéro de téléphone</label>
                                <input style="margin-bottom:1rem" type="text" class="form-control" id="callingNumber" aria-describedby="emailHelp">
                                <div id="calling-number-submit">
                                <div id="recaptcha-container"></div>
                                <button id="submit-number" type="button" class="btn  submit-number-button btn-primary" >Valider</button>
                                <div style="margin-top:0.5rem" id="submit-number-counter">10</div>
                                </div>
                            </form>
                        `);
                        // Create a Recaptcha verifier instance globally
                        // Calls submitPhoneNumberAuth() when the captcha is verified
                        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
                            "submit-number", {
                                size: "invisible",
                                callback: function(response) {
                                    submitPhoneNumberAuth();
                                }
                            }
                        );
                        //count the number of retrying
                        $("#submit-number").click(
                            function() {
                                let counter = 10;
                                $(this).addClass("btn-secondary");
                                $(this).text("patientez...");
                                // $(this).attr('onclick', '');
                                let value = $("#callingNumber").val()
                                setTimeout(() => {
                                    $(this).removeClass("btn-secondary");
                                    $(this).text("Réessayer");
                                }, 7000);
                                let myInterval2 = setInterval(() => {
                                    $("#submit-number-counter").text(counter);
                                    counter--;
                                }, 1000);
                                window.phoneNumber = value;
                                console.log(value);
                                $("#calling-numbersubmit").after(loading3);
                                // $("#submit-number").hide();
                                submitPhoneNumberAuth().then(() => {
                                    clearInterval(myInterval2);
                                    $(".fill-informations-container").html(`
                                    <form id="calling-number-form">
                                    <label for="verification-code">Code de vérification</label>
                                    <input style="margin-bottom:1rem" type="text" class="form-control" id="code-field" aria-describedby="emailHelp">
                                    <div><h3 id="verification-status"> </h3></div>
                                    <div id="verification-code-submit">
                                    <div id="recaptcha-container"></div>
                                    <button id="verification-code-button" type="button" class="btn  verification-code-button btn-primary" >Valider</button>
                                    </div>
                                    </form>
                                    `);
                                    $(".fill-informations-container #verification-code-button").click(
                                        function() {
                                            console.log("clicked");
                                            $(".fill-informations-container #verification-code-button").hide();
                                            $("#verification-status").after(loading3);
                                            let code = $("#code-field").val();
                                            console.log(code);
                                            window.confirmationResult.confirm(code).then(function(result) {
                                                /*running this if the calling number is successfuly confirmed by firebase*/
                                                window.user = result.user;
                                                $(".fill-informations-container verification-status").text("Patientez...")
                                                console.log("sucess");
                                                console.log(user);
                                                submitQuestionnaire().then(result => {
                                                    if (result) {
                                                        console.log("authenticated succesfully");
                                                        submitQuestionnaire().then(result => {
                                                            showResults(result);
                                                        })
                                                    }
                                                }).catch(error => {
                                                    console.log("error while authenticating");
                                                    console.log(error);
                                                });
                                            }).catch(function(error) {
                                                console.log(error);
                                                $(".fill-informations-container verification-status .load-wrapp").remove();
                                                $(".fill-informations-container verification-status").text("Code erroné Réessayez ou changez de numéro")
                                            });
                                        }
                                    )
                                }).catch(error => {
                                    console.log(error);
                                });
                            }
                        )
                    } else {
                        submitQuestionnaire().then(result => {
                            showResults(result);
                        })
                    }
                }, 3000);
            }
        })
    });


    //script for searching adverts
    $("#search-adverts").click(function() {
        console.log("searching");
        $(this).prop('disabled', true);
        let data = {
            homme: criteriaFormValues.male,
            femme: criteriaFormValues.female,
            minAge: parseInt(criteriaFormValues.leftValue),
            maxAge: parseInt(criteriaFormValues.rightValue)
        };

        function loadAdverts() {
            gsap.fromTo(".adverts-container", { opacity: 1 }, {
                opacity: 0,
                duration: 1,
                onComplete: function() {
                    function fetchAdverts() {
                        window.criterias = {...data };
                        $(".adverts-container").remove();
                        fetch(baseUrl + "/api/publicites", {
                            method: "POST",
                            headers: {
                                'Content-Type': 'application/json;charset=utf-8'
                            },
                            body: JSON.stringify(data)
                        }).then(response => {
                            return response.text();
                        }).then(function(html) {
                            var parser = new DOMParser();
                            var doc = parser.parseFromString(html, 'text/html');
                            let advertContainer = doc.querySelector(".adverts-container");
                            $(".advert-instruction").css("display", "block");
                            $("#loading-adverts").remove();
                            $("#criteria-form-container").after(advertContainer);
                            gsap.fromTo(".adverts-container", { opacity: 0 }, {
                                opacity: 1,
                                duration: 1
                            });
                            $(".disapear").remove();
                            $("html, body").animate({ scrollTop: 0 }, 1000);
                            setupModal();
                            return false;
                            // setupVideo();
                        })
                    }
                    //if the user is not yet authenticated load the adverts but if the user has still esplashed time to se again an advert
                    //he will be blocked at the step that he can see code
                    if (!localStorage.getItem("token")) {
                        fetchAdverts();
                    } else {
                        //if already authenticated he has to check if the user is still pending inseide the databse before to be able to see adverts
                        checkUserPending().then(result => {
                            if (result.wait && localStorage.getItem("token")) {
                                $("#loading-adverts").remove();
                                $("#criteria-form-container").after(`<div style="display: flex;flex-direction: column;align-items: center;margin-top:1.5rem">Vous devez attendre <h5><div id="waiting-time"></div></h5> pour visionner à nouveau</div>`);
                                $("#criteria-form-container").remove();
                                let seconds = result.waiting_time;
                                setInterval(() => {
                                    $("#waiting-time").html(moment.duration(seconds, "seconds").format("d [jours], h [heures], m [minutes], s [secs] "));
                                    seconds--;
                                }, 1000);
                            } else {
                                fetchAdverts();
                            }
                        });
                    }
                }
            });
        }
        $("#criteria-form-container").after(`<div class="preloader" id="loading-adverts">
        <div class="spinner"></div>
        <span id="loading-msg">Loading...</span>
         </div>
        `);
        loadAdverts();
    })

    //script for submiting the answers for questionnaire
    function submitQuestionnaire() {
        return new Promise(function(resolve, reject) {
            console.log(window);
            let data = {
                "phoneNumber": localStorage.getItem("numero") ? localStorage.getItem("numero") : window.user.phoneNumber,
                "pubId": window.pubId,
                "soumission": window.soumission,
                "sexe": window.criterias.homme == true ? 0 : 1,
                "minAge": window.criterias.minAge,
                "maxAge": window.criterias.maxAge
            };
            console.log(data);
            fetch(baseUrl + "/api/publicites/code", {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                    //add token to the header is exists
                    [localStorage.getItem("token") ? 'app_token' : null]: localStorage.getItem("token")
                },
                body: JSON.stringify(data)
            }).then((response) => {
                response.json()
                    .then((result) => {
                        console.log(result);
                        /*in case there is no token so he is not authentified*/
                        if (!localStorage.getItem("token")) {
                            localStorage.setItem("token", result.data.token);
                            localStorage.setItem("numero", result.data.numero);
                            resolve(true);
                        } else {
                            resolve(result);
                        }
                    }).catch(error => {
                        console.log(error);
                        reject(error);
                    });
            });
        });
    }

});

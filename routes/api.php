<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => ['api', 'cors'],
], function ($router) {
    //Add you routes here, for example:
    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });
    Route::post('/publicites', 'PubliciteController@findAverts');
    Route::post('/publicites/code', 'PubliciteController@findCode');
    Route::post('/publicites/remaining', 'PubliciteController@getRemainingVideos');
    Route::post('/publicites/waitingtime', 'UtilisateurController@checkUserAllowed');
});

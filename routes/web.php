<?php

use App\Config;
use App\Entreprise;
use App\Publicite;
use App\Question;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'middleware' => ['api', 'cors'],
], function ($router) {
    //Add you routes here, for example:
    Route::get('/', function () {
        // Loading images for the principale banner
        $banners = Config::where("key", "banner")->take(2)->get();
        //loading the video for the main video advert
        $maindAdvert = Config::where("key", "mainAdvert")->take(1)->get();
        //loading differents registred partners
        $partners = Config::where("key", "logo")->get();
        //loading fixed video quota to display it
        $videoQuota = Config::where("key", "videoQuota")->take(1)->get();
        //loading price after completing video qutoa
        $completionPrice = Config::where("key", "videosCompletionPrice")->take(1)->get();
        //lading advertisements
        $adverts = Publicite::with('entreprise')->get();
        //fetching question
        $questions = Question::with('possibilites')->get();

        return view('index', ['banners' => $banners, 'mainAdvert' => $maindAdvert, 'partners' => $partners, 'adverts' => $adverts, 'questions' => $questions, 'videoQuota' => $videoQuota, 'completionPrice' => $completionPrice]);
    });
    Route::get('/stream/{folder}/{file}', 'PubliciteController@stream');
    Route::get('/questionnaires/{id}', 'PubliciteController@findQuestions');

    Route::group(['prefix' => 'admin'], function () {
        Voyager::routes();
    });
});

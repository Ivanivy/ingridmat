<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfosVisionagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infos_visionages', function (Blueprint $table) {
            $table->id();
            $table->integer("score");
            $table->integer("minAge");
            $table->integer("maxAge");
            $table->unsignedBigInteger("code_id");
            $table->unsignedBigInteger("utilisateur_id");
            $table->unsignedBigInteger("publicite_id");
            $table->foreign("code_id")->references("id")->on("codes");
            $table->foreign("utilisateur_id")->references("id")->on("utilisateurs");
            $table->foreign("publicite_id")->references("id")->on("publicites");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infos_visionages');
    }
}

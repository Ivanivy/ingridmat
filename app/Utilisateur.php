<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Utilisateur extends Model
{
    //
    protected $fillable = ['numero', 'age', 'token', 'date_creation', 'wait_time', 'sexe'];

    public function infosVisionage()
    {
        return $this->hasMany(InfosVisionage::class);
    }
}

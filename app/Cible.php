<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cible extends Model
{
    //
    public function publicites()
    {
        return $this->belongsTo(Publicite::class);
    }
}

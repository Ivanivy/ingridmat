<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    //

    protected $fillable = ['titre'];

    public function publicite()
    {
        return $this->belongsTo(Publicite::class);
    }
    public function questions()
    {
        return $this->hasMany(Question::class);
    }
}

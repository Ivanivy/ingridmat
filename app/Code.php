<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    //
    protected $fillable = ['code', 'used'];

    public function publicite()
    {
        return $this->belongsTo(Publicite::class);
    }

    public function infosVisionage()
    {
        return $this->hasOne(InfosVisionage::class);
    }
}

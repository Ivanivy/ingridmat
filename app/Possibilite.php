<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Possibilite extends Model
{
    //
    protected $fillable = ['formulation', 'correct'];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfosVisionage extends Model
{
    //
    protected $fillable = ['score', 'date', 'code_id', 'publicite_id', 'utilisateur_id', 'minAge', 'maxAge'];

    public function code()
    {
        return $this->belongsTo(Code::class);
    }

    public function publicite()
    {
        return $this->belongsTo(Publicite::class);
    }

    public function Utilisateur()
    {
        return $this->belongsTo(Utilisateur::class);
    }
}

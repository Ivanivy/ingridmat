<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use App\Utilisateur;
use Carbon\Carbon;

class UtilisateurController extends Controller
{
    //
    function checkUserAllowed(Request $request)
    {
        $app_token = $request->header('app_token');
        $validator  = Validator::make($request->all(), [
            'phoneNumber' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $utilisateur = Utilisateur::all()->where("numero", "=", $request['phoneNumber'])->first();
        if ($utilisateur && $app_token) {
            if ($utilisateur['token'] != $app_token) {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid user,not allowed'
                ], Response::HTTP_UNAUTHORIZED);
            } else {
                $remaining_seconds = strtotime($utilisateur['wait_time']) - time();
                $wait = true;
                if ($remaining_seconds > 0) {
                    $wait = true;
                } else {
                    $wait = false;
                }
                return response()->json([
                    'success' => true,
                    'message' => 'remaining videos for the user',
                    'data' => [
                        'wait' => $wait,
                        'waiting_time' => $remaining_seconds,
                    ]
                ], Response::HTTP_OK);
            }
        } else {
            return response()->json([
                'success' => false,
                'message' => "the user doesnt exists",
            ], Response::HTTP_NOT_FOUND);
        }
    }
}

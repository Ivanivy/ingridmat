<?php

namespace App\Http\Controllers;

use App\Code;
use App\Config;
use App\InfosVisionage;
use App\Publicite;
use App\Questionnaire;
use App\Utilisateur;
use Carbon\Carbon;
use Doctrine\DBAL\Schema\View;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Iman\Streamer\VideoStreamer;

class PubliciteController extends Controller
{
    //
    public function stream($folder, $file)
    {
        $path = public_path('storage/' . $folder . '/' . $file);
        VideoStreamer::streamFile($path);
    }

    public function findAverts(Request $request)
    {
        $parameters = $request->only(['homme', 'femme', 'minAge', 'maxAge']);

        $homme = $parameters['homme'];
        $femme = $parameters['femme'];
        $minAge = $parameters['minAge'];
        $maxAge = $parameters['maxAge'];
        $criteria = array();
        $expression1 = "false";
        $expression2 = "false";

        if ($homme == true) {
            $expression1 = "(sexe=0 and minAge>=? and maxAge<=?)";
            array_push($criteria, $minAge);
            array_push($criteria, $maxAge);
        }

        if ($femme == true) {
            $expression2 = "(sexe=1 and minAge>=? and maxAge<=?)";
            array_push($criteria, $minAge);
            array_push($criteria, $maxAge);
        }

        $adverts = Publicite::with('entreprise')->get();
        $filteredAdverts = array();
        foreach ($adverts as $advert) {
            $matchedTarget = $advert->cibles()->whereRaw($expression1 . " or " . $expression2, $criteria)->get();
            if (count($matchedTarget) > 0) {
                array_push($filteredAdverts, $advert);
            }
        }
        // return $filteredAdverts;
        return view('components.advertsList', ['adverts' => $filteredAdverts]);
    }

    public function findQuestions(Request $request, $id)
    {
        $app_token = $request->header('app_token');
        if ($app_token) {
            //check if the user has already watched the video and valitated it
            $utilisateur =  Utilisateur::all()->where("token", "=", $app_token)->first();
            $infosVisionage = $utilisateur->infosVisionage()->get()->where("score", ">=", "100")->where("publicite_id", "=", $id);
            if (count($infosVisionage) > 0) {
                return  View('components.questionListWarning');
            }
        }
        $questionnaire = Questionnaire::with(['questions', 'questions.possibilites'])->where("publicite_id", "=", $id)->get();
        return view('components.questionList', ['questionnaire' => $questionnaire[0]]);
    }

    public function findCode(Request $request)
    {
        /*validating the request*/
        $app_token = $request->header('app_token');
        $validator  = Validator::make($request->all(), [
            'phoneNumber' => 'required',
            'pubId' => 'numeric',
            'soumission' => 'required',
            'sexe' => 'required|boolean',
            'minAge' => 'required|numeric',
            'maxAge' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $parameters = $request->only(['phoneNumber', 'pubId', 'soumission', 'age', 'sexe', 'minAge', 'maxAge']);
        /*authenticate the user i know it must not be in this controller but under the presure things make things*/
        $utilisateur = Utilisateur::all()->where("numero", "=", $parameters['phoneNumber'])->first();
        // return ["test1" => !$utilisateur, "test2" => $utilisateur && !$app_token, "test3" => $utilisateur && $app_token];

        if (!$utilisateur) { //if there is not the corresponding user register the new number
            $utilisateur = new Utilisateur(['numero' => $parameters['phoneNumber'], 'age' => $parameters['maxAge'], 'wait_time' =>  date('Y-m-d H:i:s'), 'sexe' => $parameters['sexe'], 'token' => Str::random(80)]);
            $utilisateur->save();
        } else if ($utilisateur && !$app_token) { //if the token is different may be he changed devices update the token
            // $utilisateur['token'] = Str::random(80);
            $utilisateur->save();
        } else if ($utilisateur && $app_token) {


            $remaining_seconds = strtotime($utilisateur['wait_time']) - time();
            $wait = true;
            //checking first if the user is allowed to deep into geting bonus
            //if yes so he should wait a certain time otherwise he is blocked
            if ($remaining_seconds > 0) {
                $wait = true;
                return response()->json([
                    'success' => true,
                    'message' => 'remaining videos for the user',
                    'data' => [
                        'wait' => $wait,
                        'waiting_time' => $remaining_seconds,
                    ]
                ], Response::HTTP_OK);
            } else {
                $wait = false;
            }

            //checking in second if the advert has been alredy seeen
            $infosVisionage = $utilisateur->infosVisionage()->get()->where("score", ">=", "100")->where("publicite_id", "=", $parameters['pubId']);
            if (count($infosVisionage) > 0) {
                return response()->json([
                    'success' => true,
                    'message' => "the ad has already be seen",
                    'data' => [
                        'seen_status' => true,
                    ]
                ], Response::HTTP_OK);
            }

            if ($utilisateur['token'] != $app_token) {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid user,not allowed'
                ], Response::HTTP_UNAUTHORIZED);
            } else {
                /* now process to the gift*/
                /*calculating notes from the soumission*/
                $questionnaire = Questionnaire::with(['questions', 'questions.possibilites'])->where("publicite_id", "=", $parameters['pubId'])->first();
                $fullNote = count($questionnaire['questions']);
                $soumission = $parameters['soumission'];
                $note = 0;
                $failedAnswers  = [];
                foreach ($questionnaire['questions'] as $key => $question) {
                    $possibilites =  $question['possibilites'];
                    foreach ($possibilites as $key2 => $possibilite) {
                        if ($possibilite['correct'] == 1) {
                            if (array_key_exists($question['id'], $soumission)) {
                                if ($soumission[$question['id']] == $possibilite['id']) {
                                    $note++;
                                } else {
                                    array_push($failedAnswers, ($key + 1) . ") " . $question['formulation']);
                                }
                            }
                        }
                    }
                }
                $calculatedScore =  ($note / $fullNote) * 100;
                /**end calculating notes from the soumission**/

                /*add a new infos visionage for the specific user*/
                $infosVisionage = new InfosVisionage(['score' => $calculatedScore, 'utilisateur_id' => $utilisateur['id'], 'publicite_id' => $parameters['pubId']]);
                $infosVisionage->save();
                /*end add new infos visionage for the specific user*/
                $allInfosVisionnage = $utilisateur->infosVisionage()->where('score', 100)->get();
                $latestInfosVisionage =  InfosVisionage::latest()->first();
                // return $allInfosVisionnage;
                //manage when laravel get as a signle json object
                $videoQuota = Config::where("key", "videoQuota")->take(1)->get();
                $fixedVideoQuota = $videoQuota[0]['value'];
                if ($this->computeRemainingVideos($allInfosVisionnage) == 0 && count($allInfosVisionnage) != 0 && $latestInfosVisionage['score'] == 100) {
                    /*trying to get one bonus code from the databse*/
                    $code = Code::where('used', 0)->first();
                    /*change the bonus code to used to make it no reusable*/
                    $code['used']  = 1;
                    $code->save();
                    $current_date_time =  Carbon::now()->timestamp;
                    $waiting_date_time = strtotime('+5 minutes', $current_date_time);
                    $utilisateur['wait_time']  = date('Y-m-d H:i:s', $waiting_date_time);
                    $utilisateur->save();
                    /**end getting code from the database**/
                    return ["data" => [], 'code' => $code['code'], 'score' => $calculatedScore];
                } else if ($this->computeRemainingVideos($allInfosVisionnage) != 0) {
                    return ["data" => [], 'remainingVideos' => $this->computeRemainingVideos($allInfosVisionnage), 'score' => $calculatedScore, 'failedAnswers' => $failedAnswers];
                } else {
                    return ["data" => [], 'remainingVideos' => $fixedVideoQuota, 'score' => $calculatedScore, 'failedAnswers' => $failedAnswers];
                }
            }
            /* now process to the gift*/
        }
        /*end authenticating user*/
        return response()->json([
            'success' => true,
            'message' => 'Anonymously authenticated',
            'data' => $utilisateur
        ], Response::HTTP_CREATED);
    }

    public function computeRemainingVideos($allInfosVisionnage)
    {
        $videoQuota = Config::where("key", "videoQuota")->take(1)->get();
        $fixedVideoQuota = $videoQuota[0]['value'];
        if (count($allInfosVisionnage) % $fixedVideoQuota == 0) {
            return 0;
        } else {
            return $fixedVideoQuota - count($allInfosVisionnage) % $fixedVideoQuota;
        }
    }

    public function getRemainingVideos(Request $request)
    {
        $app_token = $request->header('app_token');
        $validator  = Validator::make($request->all(), [
            'phoneNumber' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $utilisateur = Utilisateur::all()->where("numero", "=", $request['phoneNumber'])->first();
        if ($utilisateur && $app_token) {
            if ($utilisateur['token'] != $app_token) {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid user,not allowed'
                ], Response::HTTP_UNAUTHORIZED);
            } else {
                $allInfosVisionnage = $utilisateur->infosVisionage()->where('score', 100)->get();
                return response()->json([
                    'success' => true,
                    'message' => 'remaining videos for the user',
                    'data' => [
                        'remainingVideos' => $this->computeRemainingVideos($allInfosVisionnage)
                    ]
                ], Response::HTTP_OK);
            }
        } else {
            return response()->json([
                'success' => false,
                'message' => "the user doesnt exists",
            ], Response::HTTP_NOT_FOUND);
        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //
    protected $fillable = ['formulation'];

    public function questionnaire()
    {
        return $this->belongsTo(Questionnaire::class);
    }

    public function possibilites()
    {
        return $this->hasMany(Possibilite::class);
    }
}

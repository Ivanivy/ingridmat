<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entreprise extends Model
{
    protected $fillable = ['nom', 'emplacement'];
    //
    public function publicites()
    {
        return $this->hasMany(Publicite::class);
    }
}

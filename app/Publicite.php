<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Publicite extends Model
{

    protected $fillable  = ['titre', 'date', 'url', 'type'];
    //
    public function entreprise()
    {
        return $this->belongsTo(Entreprise::class);
    }

    public function cibles()
    {
        return $this->hasMany(Cible::class);
    }

    public function codes()
    {
        return $this->hasMany(Code::class);
    }

    public function questionnaire()
    {
        return $this->hasOne(Questionnaire::class);
    }

    public function infosVisionage()
    {
        return $this->hasMany(InfosVisionage::class);
    }
}
